Build and run Tree app
==
This is example app for manipulating tree structure. It's composed from REST API based on Spring Boot and frontend written in Vue.js. 

For simplification API provides flat list of nodes which contains name and reference to its parent. This allows to partition the tree to smaller chunks when it's needed. Building and manipulation of the tree is made at the frontend side.  

Prerequisites:
--
* npm 5.6.0
* JDK 1.8

Run Tests
--
``` bash
./gradlew test
```
Run REST API
--
``` bash
./gradlew bootRun
```

Install frontend dependecies and run dev server
--

``` bash
cd frontend 
npm install
npm run dev
```

[app url: http://localhost:9090/](http://localhost:9090/)


API docs
--
[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)