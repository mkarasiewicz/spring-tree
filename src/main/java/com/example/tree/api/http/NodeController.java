package com.example.tree.api.http;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.util.List;

import javax.servlet.http.*;

import com.example.tree.application.dto.NodeDto;
import com.example.tree.application.mapper.NodeMapper;
import com.example.tree.application.service.NodeService;
import com.example.tree.domain.Node;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class NodeController {
    
    @Autowired
    private NodeService nodeService;

    @Autowired
    private NodeMapper mapper;

    @RequestMapping(value ="/nodes", method = GET)
    @ResponseStatus(OK)
    public List<NodeDto> getNodes() {

        List<Node> nodes = nodeService.getAll();
        return this.mapper.toNodeDtos(nodes);
    }

    @RequestMapping(
        value ="/nodes", 
        method = POST,
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"}
        )
    @ResponseStatus(CREATED)
    public NodeDto createNode(@RequestBody NodeDto nodeDto, HttpServletRequest request, HttpServletResponse response) {
        Node createdNode = nodeService.createNode(nodeDto);

        return this.mapper.toNodeDto(createdNode);
    }

	@RequestMapping(
        value ="/nodes/{id}", 
        method = PUT,
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"}
        )
    @ResponseStatus(OK)
    public NodeDto updateNode(@RequestBody NodeDto nodeDto, HttpServletRequest request, HttpServletResponse response) {
        Node updatedNode = nodeService.updateNode(nodeDto);

        return this.mapper.toNodeDto(updatedNode);
    }

    @RequestMapping(
        value ="/nodes/{id}", 
        method = DELETE,
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"}
        )
    @ResponseStatus(NO_CONTENT)
    public void deleteNode(@PathVariable("id") Long id) {
        nodeService.deleteNode(id);
    }
}