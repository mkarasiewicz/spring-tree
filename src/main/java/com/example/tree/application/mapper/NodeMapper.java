package com.example.tree.application.mapper;

import com.example.tree.application.dto.NodeDto;
import com.example.tree.domain.Node;
import com.example.tree.infrastructure.repository.NodeRepository;

import java.util.List;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class NodeMapper {
    @Autowired
    NodeRepository nodeRepository;

    @Mappings({
        @Mapping(source = "node.id", target = "id"),
        @Mapping(source = "node.parent.id", target = "parentId"),
        @Mapping(source = "node.name", target = "name")
    })
    public abstract NodeDto toNodeDto(Node node);

    public abstract void updateNodeFromDto(NodeDto nodeDto, @MappingTarget Node node);

    public abstract List<NodeDto> toNodeDtos(List<Node> nodes);


    @Mappings({
        @Mapping(target = "parent", ignore = true)
    })
    public Node toNode(NodeDto nodeDto) {
        if ( nodeDto == null ) {
            return null;
        }
        Node node = null;

        if(nodeDto.getId() != null) {
            node = this.nodeRepository.findOne(nodeDto.getId());
        }

        if(node == null){
            node = new Node();
        }
        Node parentNode = this.nodeRepository.findOne(nodeDto.getParentId());
        node.setParent( parentNode );
        node.setName( nodeDto.getName() );

        return node;
    }
}