package com.example.tree.application.dto;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.*;

import java.util.Objects;

public class NodeDto {

    @Id
    @NotNull
    private Long id;

    @NotNull
    private Long parentId;

    @NotNull
    private String name;

    public NodeDto() {

    }

    @JsonCreator
    public NodeDto(
        @JsonProperty("id") Long id,
        @JsonProperty("name") String name, 
        @JsonProperty("parentId") Long parentId
        ) 
    {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the parentId
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NodeDto)) return false;
        NodeDto nodeDto = (NodeDto) o;
        return Objects.equals(getId(), nodeDto.getId()) &&
                Objects.equals(getParentId(), nodeDto.getParentId()) &&
                Objects.equals(getName(), nodeDto.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getParentId(), getName());
    }
}