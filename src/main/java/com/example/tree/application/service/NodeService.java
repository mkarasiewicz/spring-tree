package com.example.tree.application.service;

import java.util.List;

import com.example.tree.application.dto.NodeDto;
import com.example.tree.application.mapper.NodeMapper;
import com.example.tree.domain.Node;
import com.example.tree.infrastructure.repository.NodeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NodeService {

    @Autowired
    private NodeRepository nodeRepository;

    @Autowired
    private NodeMapper nodeMapper;

    public Node getNode(long id) {
        return nodeRepository.findOne(id);
    }

    public List<Node> getAll() {
        return nodeRepository.findAll();
    }

    public Node createNode(NodeDto nodeDto) {
        Node node = nodeMapper.toNode(nodeDto);

        return nodeRepository.save(node);
    }

    public Node updateNode(NodeDto nodeDto) {
        Node node = nodeRepository.getOne(nodeDto.getId());
        Node parentNode = nodeRepository.getOne(nodeDto.getParentId());

        nodeMapper.updateNodeFromDto(nodeDto, node);
        node.setParent(parentNode);
        
        return nodeRepository.save(node);
    }

    public void deleteNode(Long id) {
        nodeRepository.delete(id);
    }
}