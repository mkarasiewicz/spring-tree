package com.example.tree.application.exception;

import org.springframework.dao.*;

import static org.springframework.http.HttpStatus.*;

import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
class InvalidNodeDataException {
    @ResponseStatus(value=BAD_REQUEST, reason="Invalid data passed")
    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    public void handleInvalidData() {
    }

    @ResponseStatus(value=BAD_REQUEST, reason="Invalid parent passed")
    @ExceptionHandler(DataIntegrityViolationException.class)
    public void handleInvalidParentData() {
    }

    @ResponseStatus(value=NOT_FOUND, reason="Node not found")
    @ExceptionHandler(EntityNotFoundException.class)
    public void handleNodeNotFound() {
    }
}