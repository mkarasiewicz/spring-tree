package com.example.tree.infrastructure.repository;

import java.util.List;

import com.example.tree.domain.Node;
import org.springframework.data.jpa.repository.JpaRepository;


public interface NodeRepository extends JpaRepository<Node, Long> {

    List<Node> findByName(String name);
}