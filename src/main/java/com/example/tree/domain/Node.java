package com.example.tree.domain;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "node")
public class Node {
    @Id
    @GeneratedValue()
    private long id;

    @Column()
    private String name;

    @JoinColumn(name="node_parent_id", referencedColumnName = "id")
    @ManyToOne()
    private Node parent;


    @OneToMany(mappedBy = "parent", cascade = {CascadeType.ALL})
    private Set<Node> childrens;

    public Node() {

    }
    
    public Node(String name, Node parent) {
        this.name = name;
        this.parent = parent;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }
}