package com.example.tree.api.http;

import com.example.tree.application.dto.NodeDto;
import com.example.tree.application.mapper.NodeMapper;
import com.example.tree.application.service.NodeService;
import com.example.tree.domain.Node;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(NodeController.class)
public class NodeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NodeService nodeService;

    @MockBean
    private NodeMapper mapper;


    @Test
    public void shouldReturnAllNodes_WhenNodesExists() throws Exception {

        //Given
        Node root = getRootNode();
        Node child = getNode(Long.valueOf(2), "child", root);

        treeHasNodes(root, child);

        RequestBuilder request =  get("/nodes")
                .contentType(MediaType.APPLICATION_JSON);

        //When
        ResultActions result = mockMvc.perform(request);

        //Then
        result.andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$.[0]name", containsString(root.getName())))
            .andExpect(jsonPath("$.[0]parentId", nullValue()))
            .andExpect(jsonPath("$.[1]name", containsString(child.getName())))
            .andExpect(jsonPath("$.[1]parentId", is(root.getId().intValue())));
    }


    @Test
    public void shouldCreateNode_WhenValidDataPassed() throws Exception {

        //Given
        Node newNode = getNode(Long.valueOf(2), "newNode", getRootNode());

        given(mapper.toNodeDto(newNode)).willReturn(toNodeDto(newNode));
        when(nodeService.createNode(eq(toNodeDto(newNode)))).thenReturn(newNode);

        RequestBuilder request =  post("/nodes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(toNodeDto(newNode)));
        //When
        ResultActions result = mockMvc.perform(request);

        //Then
        assertNodeJsonResponse(result, status().isCreated(), newNode);
    }

    @Test
    public void shouldUpdateNode_WhenValidDataPassed() throws Exception {

        //Given
        Node updatedNode = getNode(Long.valueOf(2), "updatedNode", getRootNode());

        given(mapper.toNodeDto(updatedNode)).willReturn(toNodeDto(updatedNode));
        when(nodeService.updateNode(eq(toNodeDto(updatedNode)))).thenReturn(updatedNode);

        RequestBuilder request =  put("/nodes/{id}", updatedNode.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(toNodeDto(updatedNode)));
        //When
        ResultActions result = mockMvc.perform(request);

        //Then
        assertNodeJsonResponse(result, status().isOk(), updatedNode);

    }


    @Test
    public void shouldDeleteNode_IfExists() throws Exception {
        //Given
        Long deletedNodeId = 2L;

        doNothing().when(nodeService).deleteNode(eq(deletedNodeId));

        RequestBuilder request =  delete("/nodes/{id}", deletedNodeId)
                .contentType(MediaType.APPLICATION_JSON);

        //When
        ResultActions result = mockMvc.perform(request);

        //Then
        result.andExpect(status().isNoContent());
    }

    private void assertNodeJsonResponse(ResultActions result, ResultMatcher status, Node node) throws Exception{

        result.andExpect(status)
                .andExpect(jsonPath("$.name", containsString(node.getName())))
                .andExpect(jsonPath("$.parentId", is(node.getParent().getId().intValue())));
    }

    private List<Node> treeHasNodes(Node... nodesArray) {
        List<Node> nodes = Arrays.asList(nodesArray);
        List<NodeDto> nodeDtos = nodes.stream().map(node -> toNodeDto(node)).collect(Collectors.toList());

        given(nodeService.getAll()).willReturn(nodes);
        given(mapper.toNodeDtos(nodes)).willReturn(nodeDtos);

        return nodes;
    }

    private Node getRootNode() {
        return getNode(Long.valueOf(1), "root", null);
    }

    private Node getNode(Long id, String name, Node parent) {
        Node node = new Node(name, parent);
        node.setId(id);

        return  node;
    }

    private NodeDto toNodeDto(Node node) {
        Long parentId = null;

        if( node.getParent() != null ) {
            parentId = node.getParent().getId();
        }

        return new NodeDto(node.getId(), node.getName(), parentId);
    }

    private String toJson(Object r) throws Exception {
        ObjectMapper map = new ObjectMapper();

        return map.writeValueAsString(r);
    }
}